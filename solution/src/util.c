#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  va_end (args);
  exit(1);
}

