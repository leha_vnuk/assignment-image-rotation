#ifndef VIEW_HEADER_ROTATE_PICTURE_H
#define VIEW_HEADER_ROTATE_PICTURE_H

#include <stdio.h>
#include <stdlib.h>

struct image rotate(struct image const*  picture);

#endif //VIEW_HEADER_ROTATE_PICTURE_H
