#ifndef _UTIL_H_
#define _UTIL_H_


#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

_Noreturn void err( const char* msg, ... );

#endif
